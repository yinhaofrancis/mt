//
//  SimpleMeshLoader.swift
//  mt
//
//  Created by Francis on 2018/1/23.
//  Copyright © 2018年 Francis. All rights reserved.
//

import Foundation
import simd

public struct MeshVertex{
    public var postion:float3
    public var uv:float2
    public var normal:float3
}


public class Mesh{
    var url:URL
    
    public init(url:URL) {
        self.url = url
    }
    open func read(){
        let read = InputStream(url: url)
    }
}
public enum lineType{
    case Annotation
    case DisplayRenderAttributes(String)
    case Group
    case smoothGroup
    case Vertex
    case VertexTexture
    case VertexNormal
    case Point
    case Line
    case Face
    case unknow
}
public class MeshParser{
    var url:URL
    
    public init(url:URL) {
        self.url = url
    }
    open func parse(){
        let read = InputStream(url: self.url)
        read?.open()
    }
    fileprivate func checkLine(read:InputStream)->lineType{
        let word = self.readTo(read: read, targetChar: " ")
        if word == "g"{
            return .Group
        }
        if word == "v"{
            return .Vertex
        }
        if word == "vt"{
            return .VertexTexture
        }
        if word == "vn"{
            return .VertexNormal
        }
        if word == "p"{
            return .Point
        }
        if word == "l"{
            return .Line
        }
        if word == "f"{
            return .Face
        }
        if ["bevel","c_interp","d_interp","lod","usemtl","mtllib","shadow_obj","trace_obj","ctech","stech"].contains(word){
            return.DisplayRenderAttributes(word)
        }
        if word == "s"{
            return .smoothGroup
        }
        return .unknow
    }
    fileprivate func readTo(read:InputStream,targetChar:Character)->String{
        var char:UInt8 = 0
        var chars:[UInt8] = []
        while read.hasBytesAvailable {
            read.read(&char, maxLength: 1)
            if Character(Unicode.Scalar.init(char)) == targetChar{
                break
            }else{
                chars.append(char)
            }
        }
        return String(cString: chars)
    }
    fileprivate func parseVertex(str:String)->float3{
        return float3(str.components(separatedBy: ",").map{Float($0)!})
    }
    fileprivate func parseUV(str:String)->float2{
        return float2(str.components(separatedBy: ",").map{Float($0)!})
    }
    fileprivate func parseVertexNormal(str:String)->float3{
        return float3(str.components(separatedBy: ",").map{Float($0)!})
    }
}


