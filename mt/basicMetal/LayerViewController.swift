//
//  LayerViewController.swift
//  mt
//
//  Created by Francis on 2018/1/18.
//  Copyright © 2018年 Francis. All rights reserved.
//

import UIKit
import MetalKit
let renderScale:Double = Double(UIScreen.main.nativeScale)
@available(iOS 10.0, *)
class LayerViewController: UIViewController {
    var c:Float = 0
    lazy var link:CADisplayLink = {
        return CADisplayLink(target: self, selector: #selector(draw))
    }()
    lazy var mtlayer:CAMetalLayer = {
        return self.makeLayer()
    }()
    func makeLayer()->CAMetalLayer{
        let layer = CAMetalLayer()
        layer.device = self.device
        layer.pixelFormat = .bgra8Unorm_srgb
        layer.framebufferOnly = true
        layer.contentsScale = CGFloat(renderScale)
        return layer
    }
    lazy var mtlayer2:CAMetalLayer = {
       return self.makeLayer()
    }()
    lazy var depthTextture:MTLTexture = {
        let td = MTLTextureDescriptor()
        td.width = Int(UIScreen.main.bounds.width * UIScreen.main.nativeScale)
        td.height = Int(UIScreen.main.bounds.height * UIScreen.main.nativeScale)
        td.usage = .renderTarget
        td.depth = 1
        td.pixelFormat = .depth32Float_stencil8
        td.textureType = .type2D
        return self.device.makeTexture(descriptor: td)!
    }()
    lazy var renderPass:MTLRenderPassDescriptor = {
        let rp = MTLRenderPassDescriptor()
        rp.colorAttachments[0].clearColor = MTLClearColor(red: 0, green: 0, blue: 1, alpha: 1);
        rp.colorAttachments[0].loadAction = .clear
        rp.colorAttachments[0].storeAction = .store
        
//        rp.colorAttachments[1].clearColor = MTLClearColor(red: 0, green: 1, blue: 1, alpha: 1);
//        rp.colorAttachments[1].loadAction = .clear
//        rp.colorAttachments[1].storeAction = .store
        
        rp.depthAttachment.clearDepth = 32
        rp.depthAttachment.loadAction = .clear
        rp.depthAttachment.storeAction = .store
        rp.depthAttachment.texture = self.depthTextture
        rp.stencilAttachment.loadAction = .clear
        rp.stencilAttachment.storeAction = .store
        rp.stencilAttachment.texture = self.depthTextture
        return rp
    }()
    let device:MTLDevice = MTLCreateSystemDefaultDevice()!
    lazy var conmmadQueue:MTLCommandQueue = {
        return self.device.makeCommandQueue()!
    }()
    lazy var loader:MTKTextureLoader = {
        let l = MTKTextureLoader(device: self.device)
        
        return l;
    }()
    lazy var computeState:MTLComputePipelineState? = {
        let function = self.lib?.makeFunction(name: "textureProcess")
        return try! self.device.makeComputePipelineState(function: function!)
    }()
    lazy var texture:MTLTexture = {
        //       self.loader.newTexture(name: k, scaleFactor: UIScreen.main.scale, bundle: nil, optio)
        return try! self.loader.newTexture(name: "k", scaleFactor: UIScreen.main.scale, bundle: nil, options: nil)
    }()
    lazy var outTexture:MTLTexture = {
        return self.makeTexture(w: texture.width, h: texture.height)
    }()
    lazy var commandQueue:MTLCommandQueue? = {
        let queue = self.device.makeCommandQueue()
        
        return queue
    }()
    var transformState:WorldState = {
        var state = WorldState(world: simd_float4x4([
            float4([1,0,0,0]),
            float4([0,1,0,0]),
            float4([0,0,1,0]),
            float4([0,0,0,1])
            ]), camera: simd_float4x4([
                float4([1,0,0,0]),
                float4([0,1,0,0]),
                float4([0,0,1,0]),
                float4([0,0,0,1])
                ]),model:simd_float4x4([
                    float4([1,0,0,0]),
                    float4([0,1,0,0]),
                    float4([0,0,1,0]),
                    float4([0,0,0,1])
                    ]))
        let ratio = UIScreen.main.bounds.width / UIScreen.main.bounds.height
        state.setPerspective(fov: Float.pi / 4, aspect: Float(ratio), far: 10000, near: 0.1)
        state.camera *= WorldState.makeTransform(x: 0, y: -500, z: -3000)
        return state
    }()
    lazy var renderPipelineState:MTLRenderPipelineState? = {
        let descriptor = MTLRenderPipelineDescriptor()
        descriptor.colorAttachments[0].pixelFormat = .bgra8Unorm_srgb
//        descriptor.colorAttachments[1].pixelFormat = .bgra8Unorm_srgb
        descriptor.depthAttachmentPixelFormat = .depth32Float_stencil8
        descriptor.stencilAttachmentPixelFormat = .depth32Float_stencil8
        descriptor.fragmentFunction = self.lib?.makeFunction(name: "fragmentShader")
        descriptor.vertexFunction = self.lib?.makeFunction(name: "vertexShader")
        return try! self.device.makeRenderPipelineState(descriptor: descriptor)
    }()
    lazy var lib:MTLLibrary? = {
        return self.device.makeDefaultLibrary()
    }()
    lazy var buffer:MTLBuffer = {
        return self.device.makeBuffer(bytes: &self.vertex, length: self.vertex.count * MemoryLayout<vertexUV>.stride, options: MTLResourceOptions.storageModeShared)
        }()!
    lazy var elementBuffer:MTLBuffer = {
        return self.device.makeBuffer(bytes: &self.vertice, length: self.vertice.count * MemoryLayout<uint>.size, options: MTLResourceOptions.storageModeShared)
        }()!
    lazy var vertex:[vertexUV] = [
        vertexUV(point: float3([-200,200,200]), uv: float2([0,0])), // 0
        vertexUV(point: float3([-200,-200,200]), uv: float2([0,1])), //1
        vertexUV(point: float3([200,200,200]), uv: float2([1,0])),   // 2
        vertexUV(point: float3([200,-200,200]), uv: float2([1,1])),  //3
        
        vertexUV(point: float3([-200,200,-200]), uv: float2([1,0])), //4
        vertexUV(point: float3([-200,-200,-200]), uv: float2([1,1])), //5
        vertexUV(point: float3([200,200,-200]), uv: float2([0,0])),  // 6
        vertexUV(point: float3([200,-200,-200]), uv: float2([0,1]))  //7
    ]
    lazy var vertice:[uint] = [
        0,1,3,3,2,0,
        2,3,7,7,6,2,
        4,5,1,1,0,4,
        6,7,5,5,4,6,
        4,0,2,2,6,4,
        1,5,7,7,3,1
    ]
    lazy var threadGroupState:(size:MTLSize,count:MTLSize) = {
        let s = MTLSize(width: 16, height: 16, depth: 1)
        let c = MTLSize(width: self.texture.width / s.width, height: self.texture.height / s.height, depth: 1)
        return (s,c)
    }()
    lazy var depthStencilState:MTLDepthStencilState = {
        let dept = MTLDepthStencilDescriptor()
        dept.depthCompareFunction = .less
        dept.isDepthWriteEnabled = true
        return self.device.makeDepthStencilState(descriptor: dept)!
    }()
    func makeTexture(w:Int,h:Int)->MTLTexture{
        let desc = MTLTextureDescriptor()
        desc.usage = [.shaderRead , .shaderWrite]
        desc.pixelFormat = .bgra8Unorm_srgb
        desc.width = w
        desc.height = h
        desc.textureType = .type2D
        return (self.device.makeTexture(descriptor: desc))!
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layer.addSublayer(self.mtlayer)
        self.view.backgroundColor = UIColor.red
        self.mtlayer.frame = UIScreen.main.bounds
        self.link .add(to: RunLoop.main, forMode: .defaultRunLoopMode)
        self.view.layer.addSublayer(self.mtlayer2)
        self.mtlayer2.frame = CGRect(x: 0, y: 0, width: self.view.frame.width / 3, height: self.view.frame.height / 3);
  
    }
    @objc func draw(){
        let renderPass = self.renderPass
        if let commamdBuffer = self.commandQueue?.makeCommandBuffer(),let pipelineState = self.renderPipelineState,let cps = computeState{
            let cpen = commamdBuffer.makeComputeCommandEncoder()
            cpen?.setComputePipelineState(cps)
            cpen?.setTexture(self.texture, index: 0)
            cpen?.setTexture(self.outTexture, index: 1)
            cpen?.dispatchThreadgroups(self.threadGroupState.count, threadsPerThreadgroup: self.threadGroupState.size)
            cpen?.endEncoding()
            if let drawAble = self.mtlayer.nextDrawable(){
                renderPass.colorAttachments[0].texture = drawAble.texture
                let encoder = commamdBuffer.makeRenderCommandEncoder(descriptor: renderPass)
                encoder?.setRenderPipelineState(pipelineState)
                encoder?.setDepthStencilState(self.depthStencilState)
                encoder?.setViewport(MTLViewport(originX: 0, originY: 0, width: Double(self.view.frame.width) * renderScale, height: Double(self.view.frame.height) * renderScale, znear: -1.0, zfar: 1.0))
                
                encoder?.setVertexBuffer(self.buffer, offset: 0, index: 0)
                self.transformState.setModel(matrix: WorldState.makeRotate(angle: c, x: 0, y: 1, z: 0))
                self.c = c + 0.001
                encoder?.setVertexBytes(&self.transformState, length: MemoryLayout<WorldState>.stride, index: 1)
                
                encoder?.setFragmentTexture(self.outTexture, index: 0)
//                encoder?.setCullMode(.front)
                
                
                //            encoder?.drawPrimitives(type: .triangle, vertexStart: 0, vertexCount: 3)
                encoder?.drawIndexedPrimitives(type: .triangle, indexCount: vertice.count, indexType: .uint32, indexBuffer: self.elementBuffer, indexBufferOffset: 0)
                encoder?.endEncoding()
                commamdBuffer.present(drawAble)
//                commamdBuffer.present(draw2)
                commamdBuffer.commit()
            }
            
        }
    }
    
}
