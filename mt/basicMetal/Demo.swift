//
//  ViewController.swift
//  mt
//
//  Created by Francis on 2018/1/16.
//  Copyright © 2018年 Francis. All rights reserved.
//

import UIKit
import simd
import MetalKit
struct vertexNIn{
    var point:float3;
    var normal:float3;
}
struct lightInfo{
    var lineDirection:float3
    var base:float4
    
}

@available(iOS 10.0, *)
class Demo: UIViewController,MTKViewDelegate {
    var c:Float = 0
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
    }
    func draw(in view: MTKView) {
        if let commamdBuffer = self.commandQueue?.makeCommandBuffer(), let renderPass = view.currentRenderPassDescriptor,let pipelineState = self.renderPipelineState,let cps = computeState{
            renderPass.colorAttachments[0].clearColor = MTLClearColor(red: 0, green: 1, blue: 0, alpha: 1)
            let cpen = commamdBuffer.makeComputeCommandEncoder()
            cpen?.setComputePipelineState(cps)
            cpen?.setTexture(self.texture, index: 0)
            cpen?.setTexture(self.outTexture, index: 1)
            cpen?.dispatchThreadgroups(self.threadGroupState.count, threadsPerThreadgroup: self.threadGroupState.size)
            cpen?.endEncoding()
            let encoder = commamdBuffer.makeRenderCommandEncoder(descriptor: renderPass)
            
            encoder?.setViewport(MTLViewport(originX: 0, originY: 0, width: Double(self.view.frame.width * UIScreen.main.nativeScale), height: Double(self.view.frame.height * UIScreen.main.nativeScale), znear: -1.0, zfar: 1.0))
            
            encoder?.setVertexBuffer(self.mesh.vertexBuffers[0].buffer, offset: self.mesh.vertexBuffers[0].offset, index: 0)
            var m = self.transformState
            m.setModel(matrix: WorldState.makeRotate(angle: c, x: 0, y: 1, z: 0))
            self.c = c + 0.01
            encoder?.setVertexBytes(&m, length: MemoryLayout<WorldState>.stride, index: 1)
            
            encoder?.setFragmentTexture(self.outTexture, index: 0)
            var line = lightInfo(lineDirection: float3(1,1,1), base: float4(0.1,0.1,0.1,1))
            encoder?.setFragmentBytes(&line, length: MemoryLayout<lightInfo>.stride, index: 1)
            encoder?.setCullMode(.front)
//            encoder?.setTriangleFillMode(.lines)
            encoder?.setRenderPipelineState(pipelineState)
            encoder?.drawIndexedPrimitives(type: .triangle, indexCount: self.mesh.submeshes[0].indexCount, indexType: self.mesh.submeshes[0].indexType, indexBuffer: self.mesh.submeshes[0].indexBuffer.buffer, indexBufferOffset: self.mesh.submeshes[0].indexBuffer.offset)
            encoder?.endEncoding()
            if let drawAble = self.MTView.currentDrawable{
                commamdBuffer.present(drawAble)
            }
            commamdBuffer.commit()
            
        }
        
    }
    let device:MTLDevice? = MTLCreateSystemDefaultDevice()
    
    lazy var loader:MTKTextureLoader = {
        let l = MTKTextureLoader(device: self.device!)
        
        return l;
    }()
    lazy var computeState:MTLComputePipelineState? = {
        let function = self.lib?.makeFunction(name: "textureProcess")
        return try! self.device?.makeComputePipelineState(function: function!)
    }()
    lazy var texture:MTLTexture = {
//       self.loader.newTexture(name: k, scaleFactor: UIScreen.main.scale, bundle: nil, optio)
        return try! self.loader.newTexture(name: "k", scaleFactor: UIScreen.main.scale, bundle: nil, options: nil)
    }()
    lazy var outTexture:MTLTexture = {
        let desc = MTLTextureDescriptor()
        desc.usage = [.shaderRead , .shaderWrite]
        desc.pixelFormat = .bgra8Unorm
        desc.width = texture.width
        desc.height = texture.height
        desc.textureType = .type2D
        return (self.device?.makeTexture(descriptor: desc))!
    }()
    lazy var commandQueue:MTLCommandQueue? = {
        let queue = self.device?.makeCommandQueue()
        
        return queue
    }()
    var transformState:WorldState{
        var state = WorldState(world: simd_float4x4([
            float4([1,0,0,0]),
            float4([0,1,0,0]),
            float4([0,0,1,0]),
            float4([0,0,0,1])
            ]), camera: simd_float4x4([
                float4([1,0,0,0]),
                float4([0,1,0,0]),
                float4([0,0,1,0]),
                float4([0,0,0,1])
                ]),model:simd_float4x4([
                    float4([1,0,0,0]),
                    float4([0,1,0,0]),
                    float4([0,0,1,0]),
                    float4([0,0,0,1])
                    ]))
        let ratio = UIScreen.main.bounds.width / UIScreen.main.bounds.height
        state.setPerspective(fov: Float.pi / 4, aspect: Float(ratio), far: 1000, near: 0.1)
        state.camera *= WorldState.makeTransform(x: 0, y: 0, z: -500)
       return state
    }
    lazy var renderPipelineState:MTLRenderPipelineState? = {
        let descriptor = MTLRenderPipelineDescriptor()
        descriptor.colorAttachments[0].pixelFormat = .bgra8Unorm
//        descriptor.depthAttachmentPixelFormat = .depth32Float
        descriptor.fragmentFunction = self.lib?.makeFunction(name: "fragmentTemp")
        descriptor.vertexFunction = self.lib?.makeFunction(name: "vertexTemp")
        return try! self.device?.makeRenderPipelineState(descriptor: descriptor)
    }()
    lazy var lib:MTLLibrary? = {
        return self.device?.makeDefaultLibrary()
    }()
    
    lazy var MTView:MTKView = {
        let v = MTKView(frame: UIScreen.main.bounds, device: self.device)
        v.delegate = self
        return v
    }()
    lazy var mesh:MTKMesh = {
        return self.makeMesh()
        }()
    lazy var threadGroupState:(size:MTLSize,count:MTLSize) = {
        let s = MTLSize(width: 16, height: 16, depth: 1)
        let c = MTLSize(width: self.texture.width / s.width, height: self.texture.height / s.height, depth: 1)
        return (s,c)
    }()
    override func loadView() {
        self.view = self.MTView;
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    func makeMesh()->MTKMesh{
        
        let mesh = MDLMesh(sphereWithExtent: float3(100,100,100), segments: uint2(60,60), inwardNormals: true, geometryType: .triangles, allocator: MTKMeshBufferAllocator(device: self.device!))
//        let mesh = MDLMesh(boxWithExtent: float3(100,100,100), segments: uint3(1,1,1), inwardNormals: false, geometryType: .triangles, allocator: MTKMeshBufferAllocator(device: self.device!))
        let mmesh = try! MTKMesh(mesh: mesh, device: self.device!)
        return mmesh
//        let accet = MDLAsset(url: Bundle.main.url(forResource: "firetrunk", withExtension: "obj")!)
//        MDLMesh(
    }
    
}
struct vertexUV {
    var point:float3
    var uv:float2
}
struct WorldState {
    var world:simd_float4x4
    var camera:simd_float4x4
    var model:simd_float4x4
    mutating func setPerspective(fov:Float,aspect:Float,far:Float,near:Float){
        
        let b = 1 / tan(fov * 0.5)
        let a =  b / aspect
        let c = far / (near - far)
        let d = c * near
        self.camera =  simd_float4x4.init(rows: [
            [a,0,0,0],
            [0,b,0,0],
            [0,0,c,d],
            [0,0,-1,0]
            ])
    }
    mutating func setModel(matrix:simd_float4x4){
        self.model = matrix
    }
    static func makeTransform(x:Float,y:Float,z:Float)->simd_float4x4{
        return simd_float4x4.init(rows: [
            [1,0,0,x],
            [0,1,0,y],
            [0,0,1,z],
            [0,0,0,1]
            ])
    }
    static func makeScale(x:Float,y:Float,z:Float)->simd_float4x4{
        return simd_float4x4.init(rows: [
            [x,0,0,0],
            [0,y,0,0],
            [0,0,z,0],
            [0,0,0,1]
            ])
    }
    static func makeRotate(angle:Float,x:Float,y:Float,z:Float)->simd_float4x4{
        let c = cos(angle)
        let s = sin(angle)
        let a11 = x * x * (1 - c) + c
        let a12 = x * y * (1 - c) - z * s
        let a13 = x * z * (1 - c) + y * s
        let a21 = x * y * (1 - c) + z * s
        let a22 = y * y * (1 - c) + c
        let a23 = y * z * (1 - c) - x * s
        let a31 = x * z * (1 - c) - y * s
        let a32 = y * z * (1 - c) + x * s
        let a33 = z * z * (1 - c) + c
        return simd_float4x4.init(rows: [
            [a11,a12,a13,0],
            [a21,a22,a23,0],
            [a31,a32,a33,0],
            [0,0,0,1]
            ])
    }
    
}

