//
//  mt.metal
//  mt
//
//  Created by Francis on 2018/1/16.
//  Copyright © 2018年 Francis. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

typedef struct {
    float4 point[[position]];
    float2 uv;
}vertexOut;
typedef struct {
    float3 point;
    float2 uv;
}vertexIn;
typedef struct {
    float3 point;
    float3 normal;
}vertexNIn;
typedef struct {
    float4 point [[position]];
    float3 normal;
}vertexNOut;
typedef struct{
    float4x4 world;
    float4x4 camera;
    float4x4 model;
}worldState;
typedef struct{
    float3 lineDirection;
    float4 base;
    
}lightInfo;
vertex vertexOut vertexShader(constant vertexIn* pointer [[buffer(0)]],
                              constant worldState* state [[buffer(1)]],
                              uint indexId [[vertex_id]]){
    
    vertexIn in = pointer[indexId];
    float4 v = float4(in.point,1);
    vertexOut out;
    out.uv = in.uv;
    out.point = state->world * state->camera * state->model * v ;
    return out;
}

fragment float4 fragmentShader(vertexOut point [[stage_in]],
                               texture2d<half> colorTexture[[texture(0)]]){
    constexpr sampler texturesamper(mag_filter::linear,min_filter::linear);
    const half4 color = colorTexture.sample(texturesamper,point.uv);
    return float4(color);
}
vertex vertexNOut vertexTemp(constant vertexNIn* pointer [[buffer(0)]],
                         constant worldState* state [[buffer(1)]],
                         uint indexId [[vertex_id]]){
    vertexNOut v;
    v.point = state->world * state->camera * state->model * float4(pointer[indexId].point,1);
    v.normal = pointer[indexId].normal;
    return v;
}
fragment float4 fragmentTemp(vertexNOut location [[stage_in]],
                             constant lightInfo* info [[buffer(1)]]){
    return saturate(dot(location.normal , info->lineDirection));
}
constant half3 kRec709Luma = half3(0.2126, 0.7152, 0.0722);
kernel void textureProcess(texture2d<half,access::read> inTexture [[texture(0)]],
                           texture2d<half,access::write> outTexture [[texture(1)]],
                           uint2 gid [[thread_position_in_grid]]){
    if(gid.x >= outTexture.get_width() || gid.y >= outTexture.get_height()){
        return;
    }
    half4 color = inTexture.read(gid);
    half gray = dot(color.rgb,kRec709Luma);
    outTexture.write(half4(gray,gray,gray,1.0),gid);
}
