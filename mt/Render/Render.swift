//
//  Render.swift
//  mt
//
//  Created by Francis on 2018/1/18.
//  Copyright © 2018年 Francis. All rights reserved.
//
import UIKit
import MetalKit
@available(iOS 9.0, *)
class Render{
    let device:MTLDevice
    let commandQueue:MTLCommandQueue
    let renderState:MTLRenderPipelineState
    let samperState:MTLSamplerState
    let program:MTLLibrary
    var texture:MTLTexture?
    let computeState:MTLComputePipelineState
    let load:MTKTextureLoader
    var inner:MTLTexture?
    init(device:MTLDevice) {
        self.device = device
        self.commandQueue = device.makeCommandQueue()!
        self.program = device.makeDefaultLibrary()!
        let descript = MTLRenderPipelineDescriptor()
        descript.vertexFunction = program.makeFunction(name: "screen")
        descript.fragmentFunction = program.makeFunction(name: "fillPixel")
        descript.colorAttachments[0].pixelFormat = .bgra8Unorm_srgb
        self.renderState = try! device.makeRenderPipelineState(descriptor: descript)
        let sapD = MTLSamplerDescriptor()
        sapD.magFilter = .nearest
        sapD.minFilter = .nearest
        sapD.mipFilter = .nearest
        self.samperState = device.makeSamplerState(descriptor: sapD)!
        self.load = MTKTextureLoader(device: device)
        self.computeState = try! device.makeComputePipelineState(function: program.makeFunction(name: "process")!)
    }
    func setImage(image:UIImage){
        if let cim = image.cgImage{
            self.texture = try? self.load.newTexture(cgImage: cim, options: nil)
            let des = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: .bgra8Unorm_srgb, width: Int(image.size.width), height: Int(image.size.height), mipmapped: false)
            des.usage = [.shaderRead,.shaderWrite]
            self.inner = self.device.makeTexture(descriptor: des)
        }
        
    }
    func render(renderPass:MTLRenderPassDescriptor,drawable:MTLDrawable,w:Double,h:Double){
        guard let buffer = self.commandQueue.makeCommandBuffer() else{
            return
        }
        guard let img = self.texture else {
            return
        }
        guard let out = self.inner else {
            return
        }
        let compute = buffer.makeComputeCommandEncoder()
        compute?.setComputePipelineState(self.computeState)
        compute?.setTexture(img, index: 0)
        compute?.setTexture(out, index: 1)
        let config = self.make(texture: img, size: 16)
        compute?.dispatchThreadgroups(config.Grid, threadsPerThreadgroup: config.per)
        compute?.endEncoding()

        guard let encoder = buffer.makeRenderCommandEncoder(descriptor: renderPass) else{
            return
        }
        
        encoder.setViewport(MTLViewport(originX: 0, originY: 0, width: w, height: h, znear: -1, zfar: 1))
        encoder.setRenderPipelineState(self.renderState)
        encoder.setFragmentSamplerState(self.samperState, index: 0)
        encoder.setFragmentTexture(out, index: 0)
        encoder.drawPrimitives(type: .triangleStrip, vertexStart: 0, vertexCount: 4)
        encoder.endEncoding()
        buffer.present(drawable)
        buffer.commit()
    }
    func make(texture:MTLTexture,size:Int)->(Grid:MTLSize,per:MTLSize){
        let s = MTLSize(width: size, height: size, depth: 1)
        let c = MTLSize(width: texture.width / s.width + 1, height: texture.height / s.height + 1, depth: 1)
        return (c,s)
    }
    func copyImage(texture:MTLTexture,complete:@escaping (MTLTexture)->Void){
        guard let buffer = self.commandQueue.makeCommandBuffer() else{
            return
        }
        guard let blit = buffer.makeBlitCommandEncoder() else{
            return
        }
        blit.generateMipmaps(for: texture)
        blit.endEncoding()
        buffer.addCompletedHandler { (buff) in
            complete(texture)
        }
        buffer.commit()
        
    }
}
