//
//  TestViewController.swift
//  mt
//
//  Created by Francis on 2018/1/19.
//  Copyright © 2018年 Francis. All rights reserved.
//

import UIKit
import MetalKit
class TestViewController: UIViewController{
    let camera = Camera()
    let select = UISlider()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layer.addSublayer(self.camera.render.MTlayer)
//        self.loadControl()
        self.camera.render.MTlayer.frame = UIScreen.main.bounds
        self.camera.start()
    }
    func loadControl(){
        self.view.addSubview(self.select)
        self.select.maximumValue = 2
        self.select.minimumValue = 0.5
        self.select.value = 1;
        self.select.frame = CGRect(x: 30, y: 200, width: UIScreen.main.bounds.width - 60, height: 40)
        self.select.addTarget(self, action: #selector(go), for: .valueChanged)
        self.camera.render.videoState.ratio = self.select.value;
    }
    @objc func go() {
        self.camera.render.videoState.ratio = self.select.value;
    }
}
