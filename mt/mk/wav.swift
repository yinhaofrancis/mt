//
//  wav.swift
//  mt
//
//  Created by Francis on 2018/1/29.
//  Copyright © 2018年 Francis. All rights reserved.
//

import Foundation
import ImageIO
struct Wav {
    var ChunkID:Int32
    var ChunkSize:Int32
    var Format:Int32
    var fmt:FmtChunk
    var data:DataTrunk
    init(fmt:FmtChunk,data:DataTrunk) {
        self.fmt = fmt
        self.data = data
        ChunkID = 0x52494646
        Format = 0x57415645
        ChunkSize = Int32((data.data.count + 36)).bigEndian;
    }
    mutating func youSBWTL(data:Data){
        if let source = CGImageSourceCreateWithData(data as CFData, nil){
            if let image = CGImageSourceCreateImageAtIndex(source, 0, nil){
                let ps = image.bytesPerRow
                let h = image.height
                let w = image.width
                let poter = UnsafeMutablePointer<UInt8>.allocate(capacity: ps * w)
                let context = CGContext.init(data: poter, width: w, height: h, bitsPerComponent: 8, bytesPerRow: ps, space: CGColorSpaceCreateDeviceRGB(), bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue)
                context?.draw(image, in: CGRect(x: 0, y: 0, width: w, height: h))
                for i in (0..<(ps * w)){
                    self.data.data.append(poter.advanced(by: i).pointee)
                }
                poter.deinitialize()
            }
        }
        
    }
    mutating func setData(data:[UInt8]){
        self.data.data = data;
    }
//    var wavdata:Data{
//        
//    }
}
struct FmtChunk {
    var Subchunk1ID:Int32
    var Subchunk1Size:Int32
    var AudioFormat:Int16
    var NumChannels:Int16
    var SampleRate:Int32
    var ByteRate:Int32
    var BlockAlign:Int16
    var BitsPerSample:Int16
    init() {
        Subchunk1ID = 0x666d7420
        Subchunk1Size = Int32(16)
        AudioFormat = Int16(1)
        NumChannels = Int16(2)
        SampleRate = Int32(44100)
        BitsPerSample = Int16(16)
        ByteRate = Int32(44100 * 2 * 16 / 8)
        BlockAlign = Int16(2 * 16 / 8)
    }
    var truckData:Data{
        let t = self
        let size = MemoryLayout<FmtChunk>.size
        let p = UnsafeMutableRawPointer.allocate(bytes: size, alignedTo: 0)
        p.storeBytes(of: t, as: FmtChunk.self)
        return Data(bytes: p, count: size)
    }
}
struct DataTrunk {
    var Subchunk2ID:Int32
    var Subchunk2Size:Int32
    var data:[UInt8]
    init(data:[UInt8]) {
        Subchunk2ID = 0x64617461
        let extra = 4 - (data.count % (2 * 2))
        self.Subchunk2Size = Int32(data.count + extra)
        self.data = data
        for _ in 0..<extra {
            self.data.append(0)
        }
    }
//    var truckData:Data{
//        struct innerDaya{
//            var Subchunk2ID:Int32
//            var Subchunk2Size:Int32
//        }
//        let inner = innerDaya(Subchunk2ID: self.Subchunk2ID, Subchunk2Size: self.Subchunk2Size)
//        let size = 8 + self.data.count
//        
//        var d = Data(capacity: size)
//    
//    }
}


