//
//  camera.swift
//  mt
//
//  Created by Francis on 2018/1/24.
//  Copyright © 2018年 Francis. All rights reserved.
//

import AVFoundation
import MetalKit
struct VideoState{
    var ratio:Float
}
struct yuvInfo{
    var w:Int;
    var h:Int;
    var pr:Int;
};
let pixelformat:MTLPixelFormat = .bgra8Unorm_srgb
@available(iOS 8.3, *)
class CameraRender{
    var videoState:VideoState = VideoState(ratio: 1)
    var device:MTLDevice = MTLCreateSystemDefaultDevice()!
    lazy var imageProcess:ImageProcess = {
        return ImageProcess(render: self)
    }()
    lazy var lib:MTLLibrary = {
        return self.device.makeDefaultLibrary()!
    }()
    lazy var state:MTLRenderPipelineState =  {
        let descriptor = MTLRenderPipelineDescriptor()
        descriptor.vertexFunction = self.lib.makeFunction(name: "screen")
        descriptor.fragmentFunction = self.lib.makeFunction(name: "fillPixel")
        descriptor.colorAttachments[0].pixelFormat = pixelformat
        return try! self.device.makeRenderPipelineState(descriptor: descriptor)
    }()
    lazy var MTlayer:CAMetalLayer = {
        let m = CAMetalLayer()
        m.contentsScale = UIScreen.main.nativeScale
        m.pixelFormat = pixelformat
        return m
    }()
    lazy var renderPassDescripter:MTLRenderPassDescriptor = {
        let renderPass = MTLRenderPassDescriptor()
        renderPass.colorAttachments[0].clearColor = MTLClearColor(red: 0, green: 0, blue: 0, alpha: 1)
        renderPass.colorAttachments[0].loadAction = .clear
        renderPass.colorAttachments[0].storeAction = .store
        return renderPass
    }()
    lazy var depthStencilState:MTLDepthStencilState = {
        let dept = MTLDepthStencilDescriptor()
        dept.depthCompareFunction = .less
        dept.isDepthWriteEnabled = true
        return self.device.makeDepthStencilState(descriptor: dept)!
    }()
    var currentRenderInfo:(MTLRenderPassDescriptor,CAMetalDrawable?){
        let drawable = self.MTlayer.nextDrawable()
        self.renderPassDescripter.colorAttachments[0].texture = drawable?.texture
        return (self.renderPassDescripter,drawable)
    }
    lazy var queue:MTLCommandQueue = {
        return self.device.makeCommandQueue()!
    }()
    func draw(image:CVPixelBuffer){
        guard let buffer = self.queue.makeCommandBuffer() else{
            return
        }
        let info = self.currentRenderInfo
        CVPixelBufferLockBaseAddress(image, CVPixelBufferLockFlags.readOnly)
        if CVPixelBufferIsPlanar(image){
//            let count = CVPixelBufferGetPlaneCount(image)
//            for i in 0..<count{
//                print(CVPixelBufferGetBaseAddressOfPlane(image, i) ?? "0x0")
//                print(CVPixelBufferGetWidthOfPlane(image, i))
//                print(CVPixelBufferGetHeightOfPlane(image, i))
//                print(CVPixelBufferGetBytesPerRowOfPlane(image, i))
//                print(i)
//            }
            if let p = CVPixelBufferGetBaseAddressOfPlane(image, 0){
                let w = CVPixelBufferGetWidthOfPlane(image,0)
                let h = CVPixelBufferGetHeightOfPlane(image,0)
                let pr = CVPixelBufferGetBytesPerRowOfPlane(image,0)
                imageProcess.yuvDecode(commandBuffer: buffer, fromW: w, fromH: h, perRow: pr, ptr: p, w: self.size.w, ratio: self.WHRatio)
            }
            
        }else{
            
            if let point = CVPixelBufferGetBaseAddress(image){
                let w = CVPixelBufferGetWidth(image)
                let h = CVPixelBufferGetHeight(image)
                let pr = CVPixelBufferGetBytesPerRow(image)
                imageProcess.contentMode(commandBuffer: buffer, fromW: w, fromH: h, perRow: pr, ptr: point, w: self.size.w, ratio: self.WHRatio)
            }
            
        }
        CVPixelBufferUnlockBaseAddress(image, CVPixelBufferLockFlags.readOnly)
        if let drawable = info.1,let encode = buffer.makeRenderCommandEncoder(descriptor: info.0){
            encode.setFragmentTexture(imageProcess.result, index: 0)
            encode.setVertexBytes(&self.videoState, length: MemoryLayout<VideoState>.stride, index: 0)
            encode.setRenderPipelineState(self.state)
            encode.setFragmentSamplerState(self.sampleState, index: 0)
            encode.drawPrimitives(type: .triangleStrip, vertexStart: 0, vertexCount: 4)
            encode.endEncoding()
            buffer.present(drawable)
            buffer.commit()
        }
    }
    func currentTexture(w:Int,h:Int,pr:Int,pointer:UnsafeRawPointer,pix:MTLPixelFormat = pixelformat)->MTLTexture?{
        if self.texture == nil{
            self.texture = self.makeTexture(w: w, h: h,pix: pix)
        }
        
        texture?.replace(region:MTLRegion(origin: MTLOrigin(x: 0, y: 0, z: 0), size: MTLSize(width: w, height: h, depth: 1)) , mipmapLevel: 0, withBytes: pointer, bytesPerRow: pr)
     
        return texture
    }
    var texture:MTLTexture?
    lazy var sampleState:MTLSamplerState = {
        let sap = MTLSamplerDescriptor()
        sap.magFilter = .linear
        sap.minFilter = .linear
        return self.device.makeSamplerState(descriptor: sap)!
    }()
    func makeTexture(w:Int,h:Int,pix:MTLPixelFormat = pixelformat)->MTLTexture?{
        let t = makeTextureMTLTextureDescriptor(w: w, h: h,pix: pix)
        let texture = self.device.makeTexture(descriptor: t)
        return texture
    }
    func makeTextureMTLTextureDescriptor(w:Int,h:Int,pix:MTLPixelFormat = pixelformat)->MTLTextureDescriptor{
        let t = MTLTextureDescriptor()
        t.height = h
        t.width = w
        t.pixelFormat = pix
        t.usage = [.shaderRead,.shaderWrite]
        return t
    }
    var size:(w:Int,h:Int){
        return (Int(ceil(self.MTlayer.drawableSize.width)),Int(ceil(self.MTlayer.drawableSize.height)))
    }
    var WHRatio:Float{
        return Float(self.MTlayer.drawableSize.width / self.MTlayer.drawableSize.height)
    }
}
class ImageProcess{
    weak var render:CameraRender?
    var result:MTLTexture?
    var buffer:MTLBuffer?
    init(render:CameraRender) {
        self.render = render
    }
    func contentMode(commandBuffer:MTLCommandBuffer,fromW:Int,fromH:Int,perRow:Int,ptr:UnsafeRawPointer,w:Int,ratio:Float){
        let from  = render?.currentTexture(w: fromW, h: fromH, pr: perRow, pointer:ptr,pix:.bgra8Unorm_srgb)
        if self.result == nil{
            self.result = self.render?.makeTexture(w: w, h: Int(ceil(Float(w) / ratio)))
        }
        
        let state = try! self.render!.device.makeComputePipelineState(function: self.render!.lib.makeFunction(name: "process")!)
        if let encode = commandBuffer.makeComputeCommandEncoder() , let f = from{
            encode.setComputePipelineState(state)
            encode.setTexture(f, index: 0)
            encode.setTexture(result, index: 1)
            let threadInfo = self.threadGroupState(width:self.result!.width,height:self.result!.height, tilesize: MTLSize(width: 100, height: 100, depth: 1))
            encode.dispatchThreadgroups(threadInfo.0, threadsPerThreadgroup: threadInfo.1)
            encode.endEncoding()
        }
    }
    func yuvDecode(commandBuffer:MTLCommandBuffer,fromW:Int,fromH:Int,perRow:Int,ptr:UnsafeRawPointer,w:Int,ratio:Float){

        let from = self.makeBuffer(pointer: ptr, h: fromH, pr: fromW)
        
        if self.result == nil{
            self.result = self.render?.makeTexture(w: w, h: Int(ceil(Float(w) / ratio)))
        }
        
        let state = try! self.render!.device.makeComputePipelineState(function: self.render!.lib.makeFunction(name: "yuvProcess")!)
        var info = yuvInfo(w: fromW, h: fromH, pr: perRow)
        if let encode = commandBuffer.makeComputeCommandEncoder(){
            encode.setComputePipelineState(state)
            encode.setTexture(result, index: 1)
            encode.setBuffer(from, offset: 0, index: 0)
            encode.setBytes(&info, length: MemoryLayout<yuvInfo>.stride, index: 1)
            let threadInfo = self.threadGroupState(width:fromH,height:fromW, tilesize: MTLSize(width: 100, height: 100, depth: 1))
            encode.dispatchThreadgroups(threadInfo.0, threadsPerThreadgroup: threadInfo.1)
            
            encode.endEncoding()
        }


    }
    func threadGroupState(width:Int,height:Int,tilesize:MTLSize)->(MTLSize,MTLSize) {
        let w = width % tilesize.width == 0 ? 0 : 1
        let h = height % tilesize.height == 0 ? 0 : 1
        let c = MTLSize(width: width / tilesize.width + w, height: height / tilesize.height + h, depth: 1)
        return (tilesize,c)
    }
    func makeBuffer(pointer:UnsafeRawPointer,h:Int,pr:Int)->MTLBuffer?{
        if self.buffer == nil{
            self.buffer = self.render?.device.makeBuffer(bytes: pointer, length: h * pr, options: .storageModeShared);
        }else{
            self.buffer?.contents().copyBytes(from: pointer, count: h * pr)
        }
        
        return self.buffer
    }
}
class Camera:NSObject,AVCaptureVideoDataOutputSampleBufferDelegate{
    var device:AVCaptureDevice
    var render:CameraRender = CameraRender()
    var session = AVCaptureSession()
    lazy var input:AVCaptureDeviceInput = {
        return try! AVCaptureDeviceInput(device: self.device)
    }()
    lazy var output:AVCaptureVideoDataOutput = {
        let out = AVCaptureVideoDataOutput()
        out.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String:kCVPixelFormatType_32BGRA]
        out.setSampleBufferDelegate(self, queue: DispatchQueue.main)
        return out;
    }()
    override init() {
        device = AVCaptureDevice.default(for: .video)!
        super.init()
        AVCaptureDevice.requestAccess(for: .video) { (n) in
            
        }
        session.addInput(self.input)
        session.addOutput(self.output)
        session.sessionPreset = .high
    }
    func start(){
        session.startRunning()
    }
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        self.render.draw(image: CMSampleBufferGetImageBuffer(sampleBuffer)!)
    }
    func clearTexture(){
        self.render.texture = nil
        self.render.imageProcess.result = nil
    }
}
