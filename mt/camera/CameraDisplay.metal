//
//  CameraDisplay.metal
//  mt
//
//  Created by Francis on 2018/1/24.
//  Copyright © 2018年 Francis. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;
typedef struct{
    uint w;
    uint h;
    uint pr;
}yuvInfo;
typedef struct {
    float4 position [[position]];
    float2 uv;
}vertexOut;
typedef struct {
    float ratio;
}videoChange;
float4 processPixel(float4 f){
    return f;
}
vertex vertexOut screen(uint index [[vertex_id]],constant videoChange* vc[[buffer(0)]]){
    vertexOut point [4] = {
        {float4(-1,1,0,1),float2(0,0)},
        {float4(-1,-1,0,1),float2(0,1)},
        {float4(1,1,0,1),float2(1,0)},
        {float4(1,-1,0,1),float2(1,1)}
    };
    float r = vc->ratio;
    float4x4 change = {
        {r,0,0,0},
        {0,r,0,0},
        {0,0,1,0},
        {0,0,0,1}
    };
    vertexOut out;
    out.position = point[index].position * change;
    out.uv = point[index].uv;
    return out;
}
fragment float4 fillPixel(vertexOut out [[stage_in]],
                          texture2d<float> texture [[texture(0)]],
                          sampler Sampler [[sampler(0)]]){
    return texture.sample(Sampler,out.uv);
}
kernel void process (texture2d<float,access::sample> inTexture [[texture(0)]],
                     texture2d<float,access::write> outTexture [[texture(1)]],
                     uint2 grid [[thread_position_in_grid]]){
    constexpr sampler imageSample(mag_filter::linear,min_filter::linear,mip_filter::linear,address::clamp_to_zero);
    if (grid.x > outTexture.get_width() || grid.y > outTexture.get_height()){
        return;
    }
    float ratio = float(inTexture.get_height()) / float(outTexture.get_width());
    float2 targetSize = float2(inTexture.get_height(),inTexture.get_width()) / ratio;
    float2 sourceSize = float2(outTexture.get_width(),outTexture.get_height());
    float2 result = (sourceSize - targetSize) / 2;
    uint2 offset = uint2(result.x,result.y);
    
    float2 pick = float2(grid.y,outTexture.get_width() - grid.x) * ratio / float2(inTexture.get_width() , inTexture.get_height());
    outTexture.write(processPixel(inTexture.sample(imageSample,pick)),grid + offset);
}

